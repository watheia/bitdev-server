# This file is a template, and might need editing before it works on your project.
FROM node:14.16-alpine

# Uncomment if use of `process.dlopen` is necessary
apk add --no-cache libc6-compat

ARG waweb_uid=1000
ARG waweb_gid=1001


ENV PORT 5000
EXPOSE 5000

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

# Set required CNB information
ENV CNB_USER_ID=${waweb_uid}
ENV CNB_GROUP_ID=${waweb_gid}

# Install packages that we want to make available at both build and run time
RUN apk add --update --no-cache bash ca-certificates

# Create user and group
RUN addgroup -g ${waweb_gid} waweb && \
  adduser -u ${waweb_uid} -G waweb -s /bin/bash -D waweb

RUN npm install --global yarn @teambit/bvm && bvm install

WORKDIR /waweb
COPY . .

CMD [ "bit", "start" ]
